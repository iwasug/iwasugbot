﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iwasugbot
{
    public class Perintah
    {
        public static string Barcode(string Docno, string Tipe)
        {
            if (Tipe == "1")
            {
                string pass = Docno.PadLeft(5, '0') + "9999";
                return Caesar_Encrypt_AllNumeric(pass);
            }
            else
            {
                string pass = Docno.PadLeft(6, '0');
                return Caesar_Encrypt_AllNumericTAT(pass);
            }
        }


        private static string Caesar_Encrypt_AllNumeric(string PlainText)
        {
            string StringArrangementNumeric = "4702583691";
            string CipherText = "";

            //Password berupa angka hasil penjumlahan tanggal dan bulan dokumen NPB.
            //Untuk contoh ini menggunakan tanggal dan bulan saat ini
            //Dim Pass As Integer = Date.Now.Month + Date.Now.Day
            int Pass = DateTime.Now.Month + DateTime.Now.Day;

            if (Pass % 10 == 0) //<---- Revisi di sini
            {
                Pass = 5;
            }

            //Encryption here:
            for (int i = 0; i < PlainText.Length; i++)
            {
                string currentCharPlain = PlainText[i].ToString();
                string currentCharCipher = "";
                int indexCharPlain = StringArrangementNumeric.IndexOf(currentCharPlain);
                int indexCharCipher = -1;
                int NumberOfStep = Pass % StringArrangementNumeric.Length;
                if (indexCharPlain + NumberOfStep > StringArrangementNumeric.Length - 1)
                {
                    indexCharCipher = NumberOfStep - (StringArrangementNumeric.Length - 1 - indexCharPlain) - 1;
                }
                else
                {
                    indexCharCipher = indexCharPlain + NumberOfStep;
                }
                currentCharCipher = StringArrangementNumeric[indexCharCipher].ToString();
                CipherText = CipherText + currentCharCipher;
            }
            string ciphertext_checksum = CipherText;
            ciphertext_checksum = ciphertext_checksum + Pass.ToString().PadLeft(2, '0');
            //ciphertext_checksum = ciphertext_checksum.PadLeft(10, "0")

            string Checksum = "";
            int NumericChecksum = 0;
            for (int i = 0; i < ciphertext_checksum.Length; i++)
            {
                string currentCharCipher = ciphertext_checksum[i].ToString();
                int currentNumericCipher = Convert.ToInt32(currentCharCipher);
                NumericChecksum = NumericChecksum + ((i + 1) * Convert.ToInt32(currentCharCipher));
            }
            if (ciphertext_checksum.Length == 10)
            {
                //Checksum = NumericChecksum Mod 9
                Checksum = (NumericChecksum % 13).ToString();
            }
            else
            {
                Checksum = (NumericChecksum % 17).ToString();
                //Checksum = NumericChecksum Mod 10
            }
            //Checksum tidak boleh 0 agar proses encode barcode mudah
            if (Checksum == "0")
            {
                Checksum = "9";
            }
            Checksum = Checksum.PadLeft(2, '0');

            return Checksum + CipherText;

        }


        private static string Caesar_Encrypt_AllNumericTAT(string PlainText)
        {
            //Salah satunya dipakai untuk terima barang ( baca rangkuman NPB )
            //Panjang nomor NPB adalah 6 atau 7 digit, panjang kombinasi gembok adalah 4 digit
            //string StringArrangement = "1THE2QUICK3BROWN4FX5JMPS6V7LAZY8DG90";
            string StringArrangementNumeric = "4702583691";
            string CipherText = "";

            //Password berupa angka hasil penjumlahan tanggal dan bulan dokumen NPB.
            //Untuk contoh ini menggunakan tanggal dan bulan saat ini
            //Dim Pass As Integer = Date.Now.Month + Date.Now.Day
            int Pass = DateTime.Now.Month + DateTime.Now.Day;

            if (Pass % 10 == 0) //<---- Revisi di sini
            {
                Pass = 5;
            }

            //Encryption here:
            for (int i = 0; i < PlainText.Length; i++)
            {
                string currentCharPlain = PlainText[i].ToString();
                string currentCharCipher = "";
                int indexCharPlain = StringArrangementNumeric.IndexOf(currentCharPlain);
                int indexCharCipher = -1;
                int NumberOfStep = Pass % StringArrangementNumeric.Length;
                if (indexCharPlain + NumberOfStep > StringArrangementNumeric.Length - 1)
                {
                    indexCharCipher = NumberOfStep - (StringArrangementNumeric.Length - 1 - indexCharPlain) - 1;
                }
                else
                {
                    indexCharCipher = indexCharPlain + NumberOfStep;
                }
                currentCharCipher = StringArrangementNumeric[indexCharCipher].ToString();
                CipherText = CipherText + currentCharCipher;
            }
            string ciphertext_checksum = CipherText;
            ciphertext_checksum = ciphertext_checksum + Pass.ToString().PadLeft(2, '0');
            //ciphertext_checksum = ciphertext_checksum.PadLeft(10, "0")

            string Checksum = "";
            int NumericChecksum = 0;
            for (int i = 0; i < ciphertext_checksum.Length; i++)
            {
                string currentCharCipher = ciphertext_checksum[i].ToString();
                int currentNumericCipher = Convert.ToInt32(currentCharCipher);
                NumericChecksum = NumericChecksum + ((i + 1) * Convert.ToInt32(currentCharCipher));
            }
            if (ciphertext_checksum.Length == 10)
            {
                //Checksum = NumericChecksum Mod 9
                Checksum = (NumericChecksum % 13).ToString();
            }
            else
            {
                Checksum = (NumericChecksum % 23).ToString();
                //Checksum = NumericChecksum Mod 10
            }
            //Checksum tidak boleh 0 agar proses encode barcode mudah
            if (Checksum == "0")
            {
                Checksum = "9";
            }
            Checksum = Checksum.PadLeft(2, '0');
            return Checksum + CipherText + DateTime.Now.Day.ToString().PadLeft(2, '0');
        }


        public static string server = "server=192.168.2.21;user id=prog;password=ind0M4rcO^Pri$;database=monitortoko";
        public static string Reset_User(string user)
        {
            MySqlConnection db = new MySqlConnection(server);
            MySqlCommand cmd = new MySqlCommand();
            StringBuilder rd = new StringBuilder();
            string pesan = "";
            try
            {

                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                string sql = "select * from `user` where username='" + user + "';";
                cmd = new MySqlCommand(sql, db);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    pesan = "USER TIDAK TERDAFTAR";
                }
                else
                {
                    string sql1 = "update `user` set password='',ketnonaktif='' where username='" + user + "';";
                    cmd = new MySqlCommand(sql1, db);
                    cmd.ExecuteNonQuery();
                    pesan = "RESET OK";
                    //string ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //fungsi.query_sql("insert into edp_log_reset_user (Username, Ip, Tgl) values ('" + user + "','" + ip + "',now())");
                }
            }
            catch (Exception err)
            {
                pesan = err.ToString();
            }
            finally
            {
                db.Close();
            }
            return pesan;
        }
    }
}
